FROM rastasheep/ubuntu-sshd:18.04

RUN apt-get update
RUN apt-get install -y \
    mpich \
    openssh-client

WORKDIR /
RUN mkdir -p /mnt/nfs

# Password-less SSH for MPICH
RUN echo "PermitEmptyPasswords yes" >> /etc/ssh/sshd_config
RUN echo "    StrictHostKeyChecking no" >> /etc/ssh/ssh_config

RUN passwd -d root
