@ECHO OFF
docker-machine --version
docker-machine start default
docker --version
docker-compose --version
ECHO Running MPICH Cluster with 4 nodes
ECHO Copyright (C) 2019, Abhyudaya Sharma
docker-compose up -d
ECHO Opening console windows in 5 seconds...
TIMEOUT /T 5
FOR /F "tokens=* USEBACKQ" %%F IN (`docker-compose ps -q`) DO (
START CMD /C "docker exec -it %%F /bin/bash"
)
