@ECHO OFF
ECHO Killing all nodes...
docker-compose down
ECHO All nodes killed successfully
TIMEOUT /T 5
