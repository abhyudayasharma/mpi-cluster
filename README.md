# mpi-cluster

An easy way to setup an MPI cluster with Docker.

> The workdir folder is shared across the host (your machine) and all of the
> virtual machines. All programs should be written in there.

## Running the cluster

- Install [Docker Toolbox](https://download.docker.com/win/stable/DockerToolbox.exe) for Windows.
- Run (double-click) `run.bat`. Four Command prompt windows would be spawned
  each corresponding to each of the four nodes, running `bash` as `root`.

## Testing the cluster

A 'Hello World' MPI application is included to test if everything is working
as expected. To run it, from any of the opened command windows, enter the
following commands:

```bash
cd /mnt/nfs
mpicc hello-mpi.c
mpiexec -f hosts /mnt/nfs/a.out
```

You should get an output similar to:

```text
root@node-4:/mnt/nfs# mpiexec -f hosts /mnt/nfs/a.out
Hello world from processor node-4, rank 3 out of 4 processors
Hello world from processor node-1, rank 0 out of 4 processors
Hello world from processor node-3, rank 2 out of 4 processors
Hello world from processor node-2, rank 1 out of 4 processors
```

### ⚠ To kill the containers, run (double-click) `stop.bat`

## Advanced

This is NOT necessary to run the containers.
To build images: `docker build -t abhyudaya/mpi-cluster`.
